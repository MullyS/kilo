'use strict'

$(document).ready(function(){
    /*============================= Responsive ===========================================*/
    let slider = $('#slider');
    slider.height(slider.width() / 1.8);

    let cloud = $('#nextCloud'),
        slideButton = $('.slideButton');
    slideButton.width(cloud.width() / 2.14);
    slideButton.height(slideButton.width() / 1.8);

    let slideCloud = $('#slide-cloud');
    slideCloud.width(slider.width() * 0.4);
    slideCloud.height(slider.height() * 0.6);
    slideCloud.css({"left" : (slider.width() - slideCloud.width()) * 0.5,
                    "top"  : (slider.height() - slideCloud.height()) * 0.5});

    $(window).resize(function(){
        //Changing #slider height to responsively
        slider.height(slider.width() / 1.8);

        //Changing .slideButton w\h to responsively
        slideButton.width(cloud.width() / 2.14);
        slideButton.height(slideButton.width() / 1.8);

        //Changing #slide-cloud w\h and l\t => (responsively, center)
        slideCloud.width(slider.width() * 0.4);
        slideCloud.height(slider.height() * 0.6);
        slideCloud.css({"left" : (slider.width() - slideCloud.width()) * 0.5,
            "top"  : (slider.height() - slideCloud.height()) * 0.5});
    });
    /*============================= Form processing ======================================*/
    $('#sendMail').click(function(){
        $.ajax({
            url: "site/contact",
            type: "POST",
            data: {"ContactForm" : {
                    "name": $('#name').val(),
                    "email": $('#email').val(),
                    "message": $('#message').val()
                }},
            success: function (data) {
                alert(data);
            },
            error: function () {
                alert('Серверная ошибка');
            }
        });
    });
    /*============================= Slider ===============================================*/
    let next = $('#nextSlide'),
        prev = $('#prevSlide'),
        currentSlide = 0,   //current slide is first
        slides = $('#slider .slide');   //find all slides

    slides[currentSlide].className = 'slide showing';   //toggle 'showing' class for first slide

    let gotoSlide = function (n) {
        slides[currentSlide].className = 'slide';   //remove 'showing' class from current slide
        currentSlide = (n + slides.length) % slides.length; //calculating NEW current slide
        slides[currentSlide].className = 'slide showing';   //toggle 'showing' class to NEW current slide
    };

    next.click(function(){gotoSlide(currentSlide + 1)});
    prev.click(function(){gotoSlide(currentSlide - 1)});

});




