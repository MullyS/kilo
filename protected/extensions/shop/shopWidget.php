<?php

class shopWidget extends CWidget {

    public $shop;

    public function init() {
        $model = new Shop;
        $modeps = $model->findAll();
        $str = "";
        foreach ($modeps as $moped) {
            $str .= "<div class=\"moped\"> \n";
            $str .= "    <img src=\"{$moped->photo}\" alt=\"\" class=\"moped-img\"> \n";
            $str .= "    <div class=\"moped-text\"> \n";
            $str .= "        <h3>{$moped->name}</h3> \n";
            $str .= "        <p class=\"prop\">Цена(руб): <b>{$moped->price}</b></p> \n";
            $str .= "        <p class=\"prop\">Страна: <b>{$moped->land}</b></p> \n";
            $str .= "        <p class=\"prop\">Двигатель: <b>{$moped->engine}</b></p> \n";
            $str .= "        <p class=\"prop\">Мощность(л\с): <b>{$moped->power}</b></p> \n";
            $str .= "        <p class=\"prop\">Диаметр дисков(дюйм): <b>{$moped->wheel_diameter}</b></p> \n";
            $str .= "    </div> \n";
            $str .= "</div> \n";
        }
        $this->shop = $str;
    }

    public function run() {
        $this->render('shop_view', ["shop"=>$this->shop]);
    }

}