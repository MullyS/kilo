<?php

class sliderWidget extends CWidget {

    public $slides;

    public function init() {
        $model = new Slider();
        $slides = $model->findAll();
        $str = "";
        foreach ($slides as $slide) {
            $str .= "<div class=\"slide\" id=\"slide\"> \n";
            if($slide->have_text == true) {
                $str .= "<div class=\"slide-cloud\" id=\"slide-cloud\"> \n";
                $str .= "    <p class=\"slide-text\">{$slide->text}</p> \n";
                $str .= "    <a class=\"sc-btn\" href=\"/shop\" style=\"text-align: center;\">Узнать больше</a> \n";
                $str .= "</div>";
            }
            $str .= "    <img src=\"{$slide->photo}\" alt=\"\" class=\"slide-img\"> \n";
            $str .= "</div> \n";
        }
        $this->slides = $str;
    }

    public function run() {
        $this->render('slider_view', ['slides'=>$this->slides]);
    }
    
}