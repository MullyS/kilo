<section id="home-slider">
    <div class="container">
        <div class="row">
            <div class="slider" id="slider">
                <div class="cloud-prev" id="prevCloud"><div class="prev slideButton" id="prevSlide"></div></div>
                <div class="cloud-next" id="nextCloud"><div class="next slideButton" id="nextSlide"></div></div>
                <?=$slides?>
            </div>
        </div>
    </div>
</section>
<!--/#home-slider-->