<?php
/* @var $this ShopController */
/* @var $model Shop */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'shop-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'land'); ?>
		<?php echo $form->textField($model,'land',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'land'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'engine'); ?>
		<?php echo $form->textField($model,'engine',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'engine'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wheel_diameter'); ?>
		<?php echo $form->textField($model,'wheel_diameter'); ?>
		<?php echo $form->error($model,'wheel_diameter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'power'); ?>
		<?php echo $form->textField($model,'power'); ?>
		<?php echo $form->error($model,'power'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'photo'); ?>
		<?php echo $form->textField($model,'photo',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'photo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->