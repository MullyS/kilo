<?php
/* @var $this ShopController */
/* @var $data Shop */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('land')); ?>:</b>
	<?php echo CHtml::encode($data->land); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engine')); ?>:</b>
	<?php echo CHtml::encode($data->engine); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wheel_diameter')); ?>:</b>
	<?php echo CHtml::encode($data->wheel_diameter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('power')); ?>:</b>
	<?php echo CHtml::encode($data->power); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('photo')); ?>:</b>
	<?php echo CHtml::encode($data->photo); ?>
	<br />

	*/ ?>

</div>