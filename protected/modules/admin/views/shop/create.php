<?php
/* @var $this ShopController */
/* @var $model Shop */

$this->breadcrumbs=array(
	'Shops'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Shop', 'url'=>array('index')),
	array('label'=>'Manage Shop', 'url'=>array('admin')),
);
?>

<section>
    <div class="container">
        <div class="row">
            <h1>Create Shop</h1>
            <?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div>
    </div>
</section>

<style>
    .form .row{
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: center;
    }
</style>
