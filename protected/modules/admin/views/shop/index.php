<?php
/* @var $this ShopController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shops',
);

$this->menu=array(
	array('label'=>'Create Shop', 'url'=>array('create')),
	array('label'=>'Manage Shop', 'url'=>array('admin')),
);
?>

<section>
    <div class="container">
        <div class="row">
            <h1>Shops</h1>

            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataProvider,
                'itemView'=>'_view',
            )); ?>

        </div>
    </div>
</section>

<style>
    #sidebar{
        display: flex;
    }
    .items .view{
        position: relative;
    }

    .items .view::after{
        content: '';
        position: absolute;
        bottom: -5px;
        left: 0;
        display: block;
        width: 100%;
        height: 1px;
        background-color: black;
        margin: 5px 0;
    }
</style>
