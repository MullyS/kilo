<?php
/* @var $this ShopController */
/* @var $model Shop */

$this->breadcrumbs=array(
	'Shops'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Shop', 'url'=>array('index')),
	array('label'=>'Create Shop', 'url'=>array('create')),
	array('label'=>'View Shop', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Shop', 'url'=>array('admin')),
);
?>

<section>
    <div class="container">
        <div class="row">
            <h1>Update Shop <?php echo $model->id; ?></h1>

            <?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div>
    </div>
</section>

<style>
    .form .row{
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: center;
    }
</style>