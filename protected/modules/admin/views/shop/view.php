<?php
/* @var $this ShopController */
/* @var $model Shop */

$this->breadcrumbs=array(
	'Shops'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Shop', 'url'=>array('index')),
	array('label'=>'Create Shop', 'url'=>array('create')),
	array('label'=>'Update Shop', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Shop', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Shop', 'url'=>array('admin')),
);
?>

<section>
    <div class="container">
        <div class="row">
            <h1>View Shop #<?php echo $model->id; ?></h1>

            <?php $this->widget('zii.widgets.CDetailView', array(
                'data'=>$model,
                'attributes'=>array(
                    'id',
                    'name',
                    'price',
                    'land',
                    'engine',
                    'wheel_diameter',
                    'power',
                    'photo',
                ),
            )); ?>
        </div>
    </div>
</section>

<style>
    .form .row{
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: center;
    }
</style>


