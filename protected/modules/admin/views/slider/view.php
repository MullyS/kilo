<?php
/* @var $this SliderController */
/* @var $model Slider */

$this->breadcrumbs=array(
	'Sliders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Slider', 'url'=>array('index')),
	array('label'=>'Create Slider', 'url'=>array('create')),
	array('label'=>'Update Slider', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Slider', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Slider', 'url'=>array('admin')),
);
?>

<section>
    <div class="container">
        <div class="row">
            <h1>View Slider #<?php echo $model->id; ?></h1>

            <?php $this->widget('zii.widgets.CDetailView', array(
                'data'=>$model,
                'attributes'=>array(
                    'id',
                    'photo',
                    'have_text',
                    'text',
                ),
            )); ?>
        </div>
    </div>
</section>

<style>
    .form .row{
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: center;
    }
</style>


