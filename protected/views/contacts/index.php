<script type="text/javascript" defer>
    // Функция ymaps.ready() будет вызвана, когда
    // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
    ymaps.ready(init);
    function init(){
        // Создание карты.
        var myMap = new ymaps.Map("map", {
            center: [58.01630348, 56.25247395],
            zoom: 18
        });
        var myGeoObject = new ymaps.GeoObject({
            geometry: {
                type: "Point", // тип геометрии - точка
                coordinates: [58.01634247, 56.25249056] // координаты точки
            }
        });

        // Размещение точки на карте.
        myMap.geoObjects.add(myGeoObject);

    }
</script>
<section id="page-breadcrumb">
    <div class="vertical-center sun">
        <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">Контакты</h1>
                        <p>Телефон: 233-67-56</p>
                        <p>Главный менеджер: +7 952-331-19-27 [Антон]</p>
                        <p>Email: example@mail.com</p>
                        <h1 class="title" style="margin-top: 20px;">Как проехать</h1>
                        <p>Адрес: г. Пермь, ул. Максима Горького 22а</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#action-->

<section id="map-section">
    <div class="container">
        <div id="map" style="width: 100%; height: 400px"></div>
    </div>
</section> <!--/#map-section-->
