<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Mopedo::<??></title>
    <link href="<?=Yii::app()->request->baseUrl?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->baseUrl?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->baseUrl?>/css/animate.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->baseUrl?>/css/lightbox.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->baseUrl?>/css/main.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->baseUrl?>/css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="<?=Yii::app()->request->baseUrl?>/js/html5shiv.js"></script>
    <script src="<?=Yii::app()->request->baseUrl?>/js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?=Yii::app()->request->baseUrl?>/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=Yii::app()->request->baseUrl?>/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=Yii::app()->request->baseUrl?>/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=Yii::app()->request->baseUrl?>/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?=Yii::app()->request->baseUrl?>/images/ico/apple-touch-icon-57-precomposed.png">
    <script src="https://api-maps.yandex.ru/2.1/?apikey=afe27d46-400c-4812-8cef-87ab6634ea4f&lang=ru_RU" type="text/javascript"></script>
</head><!--/head-->

<body>
<header id="header">
    <div class="navbar navbar-inverse" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="/">
                    <h1><img src="<?=Yii::app()->request->baseUrl?>/images/logo1.png" alt="logo"></h1>
                </a>

            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/">Главная</a></li>
                    <li class=""><a href="/shop">Модельный ряд</a></li>
                    <li class=""><a href="/about">О нас</a></li>
                    <li class=""><a href="/contacts">Контакты</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!--/#header-->
<?=$content?>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">
                <img src="<?=Yii::app()->request->baseUrl?>/images/home/under.png" class="img-responsive inline" alt="">
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="contact-info bottom">
                    <h2>Контакты</h2>
                    <address>
                        Email: <a href="mailto:someone@example.com">example@mail.com</a> <br>
                        Телефон: 233-67-56 <br>
                        Менеджер: +7 952-331-19-27<br>
                    </address>

                    <h2>Адрес</h2>
                    <address>
                        Россия <br>
                        г. Пермь <br>
                        ул. Максима Горького 22а <br>
                    </address>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="contact-form bottom">
                    <h2>Оставить заявку</h2>
                    <form id="main-contact-form" name="contact-form" method="post" action="">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" required="required" placeholder="Имя" id="name">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" required="required" placeholder="Ваш email" id="email">
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Ваше сообщение" id="message"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="button" name="submit" class="btn btn-submit" id="sendMail" >Отправить</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Mopedo 2019. Все права защищены.</p>
                    <p>Разработан <a target="_blank" href="https://thekilo.ru/">the KILO studio</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/js/jquery.js"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/js/lightbox.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/js/wow.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/js/main.js"></script>
<script type="text/javascript" src="/js/test.js"></script>

</body>
</html>
