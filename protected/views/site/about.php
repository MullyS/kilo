<section id="page-breadcrumb">
    <div class="vertical-center sun">
        <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">О нас</h1>
                        <p>Почему наши клиенты выбирают нас.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="company-information" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="<?=Yii::app()->request->baseUrl?>/images/aboutus/1.png" class="img-responsive" alt="">
            </div>
            <div class="col-sm-6 padding-top">
                <p>Качество - один из важнейших параметров каждого хорошего заведения, или магазина</p>
                <p>Мы стараемся выпускать только лучшее мопеды, прошедшие тщательный отбор и сделанные их высококачественного металла.</p>
            </div>
        </div>
    </div>
</section>

<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                        <img src="<?=Yii::app()->request->baseUrl?>/images/home/icon1.png" alt="">
                    </div>
                    <h2>Честные сделки</h2>
                    <p>Сделки проходя под контролем оффициальныъ лиц, мы гарантируем качество и безопасность сделки.</p>
                </div>
            </div>
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                        <img src="<?=Yii::app()->request->baseUrl?>/images/home/icon2.png" alt="">
                    </div>
                    <h2>Надежная защита</h2>
                    <p>Защита от незаконных санкций, на основании подписаного документа.</p>
                </div>
            </div>
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <img src="<?=Yii::app()->request->baseUrl?>/images/home/icon3.png" alt="">
                    </div>
                    <h2>Бесплатное обслуживание</h2>
                    <p>Нашим клиентам предоставляется возможность 3 года проходить любые проверки в нащих сервисах.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#services-->
