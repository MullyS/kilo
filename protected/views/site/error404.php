<style>
    .error404{
        text-align: center;
    }
    .error404Link{
        font-size: 24px;
    }
</style>
<section id="404">
    <div class="container">
        <div class="row">
            <h1 class="error404">Ошибка 404<br>запрашиваемая страница не найдена</h1>
            <p class="error404" style="margin-top: 20px;"><a href="/" class="error404Link">>>>Вернуться на главную<<<</a></p>
        </div>
    </div>
</section>