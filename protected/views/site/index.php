<? $this->widget('ext.slider.sliderWidget'); ?>

<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                        <img src="<?=Yii::app()->request->baseUrl?>images/home/icon1.png" alt="">
                    </div>
                    <h2>Честные сделки</h2>
                    <p>Сделки проходя под контролем оффициальныъ лиц, мы гарантируем качество и безопасность сделки.</p>
                </div>
            </div>
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                        <img src="<?=Yii::app()->request->baseUrl?>images/home/icon2.png" alt="">
                    </div>
                    <h2>Надежная защита</h2>
                    <p>Защита от незаконных санкций, на основании подписаного документа.</p>
                </div>
            </div>
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <img src="<?=Yii::app()->request->baseUrl?>images/home/icon3.png" alt="">
                    </div>
                    <h2>Бесплатное обслуживание</h2>
                    <p>Нашим клиентам предоставляется возможность 3 года проходить любые проверки в нащих сервисах.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#services-->

<section id="features">
    <div class="container">
        <div class="row">
            <div class="single-features">
                <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <img src="<?=Yii::app()->request->baseUrl?>images/home/image1.png" class="img-responsive" alt="">
                </div>
                <div class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h2>Совместная работа</h2>
                    <P>Более 50 тысяч сотрудников работаю каждый день, открывая новые возможности и достигая верного результата, путем совместной работы.</P>
                </div>
            </div>
            <div class="single-features">
                <div class="col-sm-6 col-sm-offset-1 align-right wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h2>Мы экономим ресурсы</h2>
                    <P>Наши мопеды оснащены защитой от излишек выхлопа, что способствует поддержанию экосистемы.</P>
                </div>
                <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <img src="<?=Yii::app()->request->baseUrl?>images/home/image2.png" class="img-responsive" alt="">
                </div>
            </div>
            <div class="single-features">
                <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <img src="<?=Yii::app()->request->baseUrl?>images/home/image3.png" class="img-responsive" alt="">
                </div>
                <div class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h2>Международная доставка</h2>
                    <P>Мы сотрудничаем с 3 глобальными сервисами доставки. Это позволяет нам отправлять ваш транспорт, даже если вы далеко.</P>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#features-->

<section id="clients">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="clients text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                    <p><img src="<?=Yii::app()->request->baseUrl?>images/home/clients.png" class="img-responsive" alt=""></p>
                    <h1 class="title">Довольные клиенты</h1>
                    <p>Каждый год мы набираем популярность среди русских клиентов. <br> Вы делаете нас лучше, а мы стараемся для вас </p>
                </div>
                <div class="clients-logo wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="col-xs-3 col-sm-2">
                        <a href="#"><img src="<?=Yii::app()->request->baseUrl?>images/home/client1.png" class="img-responsive" alt=""></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="#"><img src="<?=Yii::app()->request->baseUrl?>images/home/client2.png" class="img-responsive" alt=""></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="#"><img src="<?=Yii::app()->request->baseUrl?>images/home/client3.png" class="img-responsive" alt=""></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="#"><img src="<?=Yii::app()->request->baseUrl?>images/home/client4.png" class="img-responsive" alt=""></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="#"><img src="<?=Yii::app()->request->baseUrl?>images/home/client5.png" class="img-responsive" alt=""></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="#"><img src="<?=Yii::app()->request->baseUrl?>images/home/client6.png" class="img-responsive" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#clients-->